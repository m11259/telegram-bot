package domain

import (
	"time"

	"gitlab.com/m11259/telegram-bot/handler"
	"gitlab.com/m11259/telegram-bot/repository"
)

func (d *Domain) onUserApproved(ctx *handler.Ctx) (string, int, bool) {
	ctx.SetMessageText("notify/user-approved")
	ctx.SetKeyboard("notify/user-approved")
	ctx.Reply()
	return "", -1, true
}

func (d *Domain) onUserDenied(ctx *handler.Ctx) (string, int, bool) {
	ctx.SetMessageText("notify/user-denied")
	ctx.SetKeyboard("notify/user-denied")
	ctx.Reply()
	return "", -1, true
}

func (d *Domain) onUserActivated(ctx *handler.Ctx) (string, int, bool) {
	ctx.SetMessageText("notify/user-activated")
	ctx.SetKeyboard("notify/user-activated")
	ctx.Reply()
	return "", -1, true
}

func (d *Domain) onUserDeactivated(ctx *handler.Ctx) (string, int, bool) {
	ctx.SetMessageText("notify/user-deactivated")
	ctx.Reply()
	return "", -1, true
}

func (d *Domain) onReqAccepted(ctx *handler.Ctx) (string, int, bool) {
	if ctx.ID() == 0 {
		return "", -1, true
	}
	req := (ctx.EventData()).(repository.Request)
	datetime, _ := time.Parse("2006-01-02T15:04:05.000Z", *req.CreatedAt)
	ctx.SetMessageText("notify/request-accepted", map[string]interface{}{
		"id":       req.Id,
		"body":     req.Body,
		"datetime": datetime.Format("15:04 02.01.2006"),
	})
	ctx.SetKeyboard("notify/request-accepted")
	ctx.Reply()
	return "", -1, true
}

func (d *Domain) onReqUpdated(ctx *handler.Ctx) (string, int, bool) {
	req := (ctx.EventData()).(repository.Request)
	datetime, _ := time.Parse("2006-01-02T15:04:05.000Z", *req.CreatedAt)
	ctx.SetMessageText("notify/request-accepted", map[string]interface{}{
		"id":       req.Id,
		"body":     req.Body,
		"datetime": datetime.Format("15:04 02.01.2006"),
	})
	ctx.Reply()
	return "", -1, true
}

func (d *Domain) onReqDenied(ctx *handler.Ctx) (string, int, bool) {
	req := (ctx.EventData()).(repository.Request)
	datetime, _ := time.Parse("2006-01-02T15:04:05.000Z", *req.CreatedAt)
	ctx.SetMessageText("notify/request-denied", map[string]interface{}{
		"id":       req.Id,
		"body":     req.Body,
		"datetime": datetime.Format("15:04 02.01.2006"),
	})
	ctx.SetKeyboard("notify/request-denied")
	ctx.Reply()
	return "", -1, true
}

func (d *Domain) onReqUnaccepted(ctx *handler.Ctx) (string, int, bool) {
	req := (ctx.EventData()).(repository.Request)
	datetime, _ := time.Parse("2006-01-02T15:04:05.000Z", *req.CreatedAt)
	ctx.SetMessageText("notify/request-unaccepted", map[string]interface{}{
		"id":       req.Id,
		"body":     req.Body,
		"datetime": datetime.Format("15:04 02.01.2006"),
	})
	ctx.SetKeyboard("notify/request-unaccepted")
	ctx.Reply()
	return "", -1, true
}

func (d *Domain) onReqUndenied(ctx *handler.Ctx) (string, int, bool) {
	req := (ctx.EventData()).(repository.Request)
	datetime, _ := time.Parse("2006-01-02T15:04:05.000Z", *req.CreatedAt)
	ctx.SetMessageText("notify/request-undenied", map[string]interface{}{
		"id":       req.Id,
		"body":     req.Body,
		"datetime": datetime.Format("15:04 02.01.2006"),
	})
	ctx.SetKeyboard("notify/request-undenied")
	ctx.Reply()
	return "", -1, true
}
