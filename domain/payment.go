package domain

import "gitlab.com/m11259/telegram-bot/handler"

func (d *Domain) getPayment(ctx *handler.Ctx) (string, int, bool) {
	ctx.SetMessageText("profile/payments/requisites")
	// ctx.SetKeyboard("user-manager/payment/start")
	ctx.Reply()
	return "", 0, false
}

func (d *Domain) sendPaymentInvoice(ctx *handler.Ctx) (string, int, bool) {
	ctx.SetPayment()
	ctx.Reply()
	return "", 0, false
}
