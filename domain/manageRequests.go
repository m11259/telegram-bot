package domain

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/m11259/telegram-bot/handler"
	"gitlab.com/m11259/telegram-bot/logger"
	"gitlab.com/m11259/telegram-bot/repository"

	omsv1 "go.buf.build/grpc/go/aipyth/ongate-oms/oms/v1"
)

const pageLength = 3
const stateName = "request-manager"

var currentPage = make(map[int64]int, 0)

func requestsList(reqs []*omsv1.Request) string {
	reqList := make([]string, len(reqs))
	for i := range reqs {
		reqList[i] = fmt.Sprintf("№%d.\n%s\n%s", reqs[i].Id, reqs[i].Body, *reqs[i].CreatedAt)

		if reqs[i].CarNumber != nil {
			reqList[i] += fmt.Sprintf("\n%s", *reqs[i].CarNumber)
		}
	}

	return strings.Join(reqList, "\n\n")
}

func displayClientRequests(ctx *handler.Ctx, request omsv1.RequestServiceClient, page int, edit bool) {
	SetMessage := ctx.SetMessageText
	if edit {
		SetMessage = ctx.SetEditMessageText
	}

	id := ctx.ID()
	reqs, err := request.RetrieveRequests(repository.Context(), &omsv1.RetrieveRequestsRequest{
		UserId: &id,
		Limit:  pageLength,
		Offset: int32(page * pageLength),
	})
	if err != nil {
		logger.Error(err)
		ctx.SetMessageText("notify/error").Reply()
		return
	}

	reqs_next, err := request.RetrieveRequests(repository.Context(), &omsv1.RetrieveRequestsRequest{
		UserId: &id,
		Limit:  pageLength,
		Offset: int32((page + 1) * pageLength),
	})
	if err != nil {
		logger.Error(err)
		ctx.SetMessageText("notify/error").Reply()
		return
	}

	if len(reqs.Requests) != 0 {
		SetMessage(stateName+"/list", map[string]interface{}{
			"requests": requestsList(reqs.Requests),
		})
		for _, req := range reqs.Requests {
			ctx.AppendInlineButtonRow(edit,
				stateName+"/buttons",
				ctx.NewButtonParams(
					2,
					map[string]interface{}{"request_id": req.Id},
				),
			)
		}
		switch {
		case page == 0 && len(reqs_next.Requests) != 0:
			ctx.AppendInlineButtonRow(edit,
				stateName+"/buttons",
				ctx.NewButtonParams(0),
			)
			break
		case len(reqs_next.Requests) != 0:
			ctx.AppendInlineButtonRow(edit,
				stateName+"/buttons",
				ctx.NewButtonParams(0),
				ctx.NewButtonParams(1),
			)
			break
		case page != 0:
			ctx.AppendInlineButtonRow(edit,
				stateName+"/buttons",
				ctx.NewButtonParams(1),
			)
			break
		}
	} else {
		ctx.SetMessageText(stateName + "/no-reqs")
	}
	ctx.Reply()
}

func (d *Domain) viewRequests(ctx *handler.Ctx) (string, int, bool) {
	displayClientRequests(ctx, d.repository.Request, 0, false)
	currentPage[ctx.ID()] = 0
	return stateName, 0, false
}

func (d *Domain) viewRequestsNextPage(ctx *handler.Ctx) (string, int, bool) {
	currentPage[ctx.ID()]++
	displayClientRequests(ctx, d.repository.Request, currentPage[ctx.ID()], true)
	return stateName, 1, false
}

func (d *Domain) viewRequestsPrevPage(ctx *handler.Ctx) (string, int, bool) {
	currentPage[ctx.ID()]--
	displayClientRequests(ctx, d.repository.Request, currentPage[ctx.ID()], true)
	return stateName, -1, false
}

func (d *Domain) cancelRequest(ctx *handler.Ctx) (string, int, bool) {
	req_id, _ := strconv.Atoi(strings.Split(ctx.Data(), "-")[2])
	del, err := d.repository.Request.DeleteRequest(repository.Context(), &omsv1.DeleteRequestRequest{
		Id: int64(req_id),
	})
	if err != nil {
		logger.Error(err)
		ctx.SetMessageText("notify/error").Reply()
		return "", 0, true
	}
	d.repository.Event.Emit(repository.RequestCancel, del)
	displayClientRequests(ctx, d.repository.Request, currentPage[ctx.ID()], true)
	return stateName, 0, false
}
