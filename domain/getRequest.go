package domain

import (
	// "encoding/json"
	"regexp"
	"strings"
	"time"

	"gitlab.com/m11259/telegram-bot/handler"
	"gitlab.com/m11259/telegram-bot/repository"
	"google.golang.org/grpc/codes"

	omsv1 "go.buf.build/grpc/go/aipyth/ongate-oms/oms/v1"
)

var requestData = make(map[int64]*repository.UserRequestData)

func processUserPermissions(user *omsv1.User) (string, bool) {
	if !user.Identified {
		return "request/intro/unidentified", false
	}

	if !user.Activated {
		return "request/intro/unactivated", false
	}
	return "", true
}

//////// HANDLE FUNCS

func (d *Domain) processRequestIntro(ctx *handler.Ctx) (string, int, bool) {
	id := ctx.ID()
	user, code, err := d.repository.GetUser(repository.GetUserOpts{Id: id})
	if err != nil {
		ctx.HandleError(err)
		return "", 0, true
	}
	switch code {
	case codes.Unavailable:
		time.Sleep(time.Second)
		return d.processRequestIntro(ctx)
	case codes.NotFound:
		ctx.SetMessageText("request/intro/unidentified")
		ctx.Reply()
		return "", 0, true
	}

	// TODO: store request data during it's creation to cache
	requestData[id] = &repository.UserRequestData{
    UserId: user.Id,
		Address: user.Address,
	}
	answer, ok := processUserPermissions(user)
	if ok {
		ctx.SetMessageText("request/intro/get-request")
		ctx.Reply()
		return "request", 1, false
	} else {
		ctx.SetMessageText(answer)
		ctx.Reply()
		return "", 0, true
	}
}

func (d *Domain) getRequestText(ctx *handler.Ctx) (string, int, bool) {
	id := ctx.ID()
	requestData := requestData[id]
	requestData.Body = ctx.Data()

	args := map[string]interface{}{
		"body":    requestData.Body,
		"address": requestData.Address,
	}
	// TODO: refactor this code cause it is repeated in processRequestText
	if d.config.Vin {
		// TODO: this two texts are almost the same except one additional kb button
		ctx.SetMessageText("request/confirm-vin", args)
		ctx.SetKeyboard("request/confirm-vin")
	} else {
		ctx.SetMessageText("request/confirm", args)
		ctx.SetKeyboard("request/confirm")
	}
	ctx.Reply()
	return "request", 1, false
}

func (d *Domain) processRequestText(ctx *handler.Ctx) (string, int, bool) {
	id := ctx.ID()

	user, code, err := d.repository.GetUser(repository.GetUserOpts{Id: id})
	if err != nil {
		ctx.HandleError(err)
		return "", 0, true
	}
	switch code {
	case codes.Unavailable:
		time.Sleep(time.Second)
		return d.processRequestText(ctx)
	case codes.NotFound:
		ctx.SetMessageText("request/intro/unidentified")
		ctx.Reply()
		return "", 0, true
	}

	requestData[id] = &repository.UserRequestData{
		Body:    ctx.Data(),
		Address: user.Address,
		UserId:  user.Id,
	}
	answer, ok := processUserPermissions(user)
	if !ok {
		delete(requestData, id)
		ctx.SetMessageText(answer)
		ctx.Reply()
		return "", 0, true
	}
	args := map[string]interface{}{
		"body":    requestData[id].Body,
		"address": requestData[id].Address,
	}
	if d.config.Vin {
		ctx.SetMessageText("request/confirm-vin", args)
		ctx.SetKeyboard("request/confirm-vin")
	} else {
		ctx.SetMessageText("request/confirm", args)
		ctx.SetKeyboard("request/confirm")
	}
	ctx.Reply()
	return "request", 2, false
}

func (d *Domain) processUserAnswer(ctx *handler.Ctx) (string, int, bool) {
	answer := ctx.Data()
	id := ctx.ID()
	requestUserData, ok := requestData[id]
  if !ok {
    ctx.SetEditMessageText("request/irrelevant")
    ctx.HideInlineKeyboard()
    ctx.Reply()
    return "", 0, true
  }
	switch answer {
	case "yes":
		req, code, err := d.repository.SendRequest(*requestUserData)
		if err != nil {
			ctx.HandleError(err)
			return "", 0, true
		}
		switch code {
		case codes.Unavailable:
			time.Sleep(time.Second)
			return d.processUserAnswer(ctx)
		}
		args := map[string]interface{}{
			"address": req.Address,
		}
		ctx.SetEditMessageText("request/accepted", args)
	case "no":
		delete(requestData, id)
		ctx.SetEditMessageText("request/denied")
	case "carnumber-add":
		return d.askUserCarnumber(ctx)
	}
	ctx.HideInlineKeyboard()
	ctx.Reply()
	return "", 0, true
}

func (d *Domain) askUserCarnumber(ctx *handler.Ctx) (string, int, bool) {
	ctx.SetMessageText("request/car-number/get")
	ctx.SetKeyboard("request/car-number/get")
  ctx.Reply()
	return "request", 1, false
}

func (d *Domain) confirmRequestCanceledCarnumber(ctx *handler.Ctx) (string, int, bool) {
	id := ctx.ID()
	args := map[string]interface{}{
		"body":    requestData[id].Body,
		"address": requestData[id].Address,
	}
	ctx.SetMessageText("request/car-number/nonumber", args)
	ctx.SetKeyboard("request/car-number/nonumber")
	ctx.Reply()
	return "request", 1, false
}

func (d *Domain) processUserCarnumber(ctx *handler.Ctx) (string, int, bool) {
	id := ctx.ID()
	vinRegex := regexp.MustCompile("([a-zа-яA-ZА-ЯІ]{2}[0-9]{4}[a-zа-яA-ZА-ЯІ]{2})")
	if !vinRegex.MatchString(ctx.Data()) {
		ctx.SetMessageText("request/car-number/invalid")
		ctx.Reply()
		return "request", 0, false
	}

  carNumber := strings.ToUpper(vinRegex.FindString(ctx.Data()))
	requestUserData := requestData[id]
	requestUserData.CarNumber = &carNumber
	args := map[string]interface{}{
		"body":      requestData[id].Body,
		"address":   requestData[id].Address,
		"carnumber": *requestData[id].CarNumber,
	}
	ctx.SetMessageText("request/car-number/confirm", args)
	ctx.SetKeyboard("request/car-number/confirm")
	ctx.Reply()
	return "request", -1, false
}
