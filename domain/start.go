package domain

import (
	"time"

	"gitlab.com/m11259/telegram-bot/handler"
	"gitlab.com/m11259/telegram-bot/logger"
	"gitlab.com/m11259/telegram-bot/repository"
	"google.golang.org/grpc/codes"
)

func (d *Domain) Start(ctx *handler.Ctx) (string, int, bool) {
	id := ctx.ID()
  user, code, err := d.repository.GetUser(repository.GetUserOpts{ Id: id })
	if err != nil {
    ctx.HandleError(err)
		return "", 0, true
	}
  logger.Debug(user, code, err)
  switch code {
  case codes.Unavailable:
    time.Sleep(time.Second)
    return d.Start(ctx)
  case codes.NotFound:
    ctx.SetMessageText("start/first-time")
    ctx.SetKeyboard("start/first-time")
    ctx.Reply()
    return "", 0, false
  }

	if user.Identified {
		ctx.SetMessageText("start/identified", map[string]interface{}{
			"name": user.Firstname,
		})
		ctx.SetKeyboard("start/identified")
	} else {
		ctx.SetMessageText("start/nonidentified", map[string]interface{}{
			"name": user.Firstname,
		})
		ctx.SetKeyboard("start/nonidentified")
	}
	ctx.Reply()
	return "", 0, false
}
