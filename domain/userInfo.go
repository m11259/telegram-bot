package domain

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/m11259/telegram-bot/handler"
	"gitlab.com/m11259/telegram-bot/logger"
	"gitlab.com/m11259/telegram-bot/repository"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	omsv1 "go.buf.build/grpc/go/aipyth/ongate-oms/oms/v1"
)

func (d *Domain) getProfile(ctx *handler.Ctx) (string, int, bool) {
	id := ctx.ID()
	user, err := d.repository.User.GetUser(repository.Context(), &omsv1.GetUserRequest{
		Id:       &id,
		Username: nil,
	})
	if status.Code(err) != codes.OK && status.Code(err) != codes.NotFound {
		logger.Error(err)
		ctx.SetMessageText("notify/error").Reply()
		return "", -1, false
	}

  lastname := ""
  if user.Lastname != nil {
    lastname = *user.Lastname
  }
  username := ""
  if user.Username != nil {
    username = *user.Username
  }

	args := map[string]interface{}{
		"firstname": user.Firstname,
		"lastname":  lastname,
		"phone":     user.Phone,
		"username":  username,
		"address":   user.Address,
		"lang":      user.Lang,
	}
	switch {
	case user.Id == 0:
		ctx.SetMessageText("profile/no-user")
	case !user.Identified:
		ctx.SetMessageText("profile/unidentified", args).
			SetKeyboard("profile/unidentified")
	case !user.Activated:
		ctx.SetMessageText("profile/identified/unactivated", args).
			SetKeyboard("profile/unidentified/unactivated")
	default:
		ctx.SetMessageText("profile/identified/activated", args).
			SetKeyboard("profile/identified/activated")
	}

	ctx.Reply()
	return "", -1, true
}

func (d *Domain) unpaidMonths(ctx *handler.Ctx) (string, int, bool) {
	var months []string
	switch ctx.Lang() {
	case "uk":
		months = []string{"Січень", "Лютий", "Березень", "Квітень", "Травень", "Червень", "Липень", "Серпень", "Вересень", "Жовтень", "Листопад", "Грудень"}
	case "en":
		months = []string{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}
	default:
		months = []string{"Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"}

	}
	id := ctx.ID()
	user, err := d.repository.User.GetUser(repository.Context(), &omsv1.GetUserRequest{
		Id:       &id,
		Username: nil,
	})
	if err != nil {
		logger.Error(err)
	}
	currentMonth := int(time.Now().Month())
	unpaid := []int{}
	// Checking payments for last 5 months
	for i := 1; i <= 5; i++ {
    // TODO: refactor this call into repository
		res, err := d.repository.Address.GetAddressPaymentsStats(repository.Context(), &omsv1.GetAddressPaymentsStatsRequest{
			Address: []string{user.Address},
			Year:    int32(time.Now().Year()),
			Month:   int32((currentMonth-i-1+len(months))%len(months)) + 1,
		})
		if err != nil {
			logger.Error(err)
		}
		if !res.Results[0].Paid {
			unpaid = append(unpaid, int(res.Month))
		}
	}
	if len(unpaid) != 0 {
		resString := make([]string, len(unpaid))
		for _, v := range unpaid {
			// item := strings.Split(unpaid[len(unpaid)-1-i], ".")
			// monthNum, - := strconv.Atoi(item[1])
			resString = append(resString, fmt.Sprintf("%d) %s", v, months[v-1]))
		}
		ctx.SetMessageText("profile/payments/unpaid", map[string]interface{}{
			"months": strings.Join(resString, "\n"),
		})
	} else {
		ctx.SetMessageText("profile/payments/ok")
	}
	ctx.Reply()

	return "", -1, true
}

func (d *Domain) initChangeLang(ctx *handler.Ctx) (string, int, bool) {
	ctx.SetMessageText("profile/lang/choose").SetKeyboard("profile/lang/choose").Reply()
	return "", -1, true
}

func (d *Domain) changeLang(ctx *handler.Ctx) (string, int, bool) {
	lang := strings.Split(ctx.Data(), "-")[2]
	code := lang[0:2]
	ctx.SetLang(code)
	ctx.SetMessageText("profile/lang/changed")
	ctx.Reply()
	return "", -1, true
}
