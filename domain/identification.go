package domain

import (
	"log"
	"regexp"
	"strings"
	"time"

	"gitlab.com/m11259/telegram-bot/handler"
	"gitlab.com/m11259/telegram-bot/logger"
	"gitlab.com/m11259/telegram-bot/repository"

	omsv1 "go.buf.build/grpc/go/aipyth/ongate-oms/oms/v1"
)

const state = "identification"

var userInfo = make(map[int64]*omsv1.CreateUserRequest)

func (d *Domain) identificationStart(ctx *handler.Ctx) (string, int, bool) {
	ctx.SetMessageText("identification/start")
	ctx.Reply()
	return state, 1, false
}

func (d *Domain) getName(ctx *handler.Ctx) (string, int, bool) {
	id := ctx.ID()
	lang := ctx.Lang()
	splitted := strings.Split(ctx.Data(), " ")
	userInfo[id] = &omsv1.CreateUserRequest{
    Id: id,
    Firstname: splitted[0],
    Lang: &lang,
  }

	if len(splitted) > 1 {
		userInfo[id].Lastname = &splitted[1]
	}

	if user := ctx.From(); user != nil {
		userInfo[id].Username = &user.UserName
		userInfo[id].Telegramname = user.FirstName + user.LastName
	}
	ctx.SetMessageText("identification/get-number")
	ctx.SetKeyboard("identification/get-number")
	ctx.Reply()
	return state, 1, false
}

func (d *Domain) getContact(ctx *handler.Ctx) (string, int, bool) {
	contact := ctx.Contact()
  reg := regexp.MustCompile("^\\+?[0-9]{2}?[0-9]{10}$")
	if reg.MatchString(contact) {
		userInfo[ctx.ID()].Phone = contact
		ctx.SetMessageText("identification/get-address").
			HideKeyboard().
			Reply()
		return state, 1, false
	}
	ctx.SetMessageText("identification/contact-not-valid").
		HideKeyboard().
		Reply()
	return state, 0, false
}

func (d *Domain) getAddress(ctx *handler.Ctx) (string, int, bool) {
	id := ctx.ID()
	userInfo[id].Address = ctx.Data()
	// all addresses from database are available in oms
	res, err := d.repository.Address.AddressExists(repository.Context(), &omsv1.AddressExistsRequest{
		Address: userInfo[id].Address,
	})
  // TODO: handle error via wrapped call in repository
	if err != nil {
		log.Fatal(err)
	}
	userInfo[id].AddressConfirmed = res.Exists
	if !userInfo[id].AddressConfirmed {
		// we notify user if he enteres address that is not present there
		ctx.SetMessageText("identification/address-not-exists").Reply()
	} else {
		// otherwise we check his last payments and activate him if he paid
		payments, err := d.repository.Address.GetAddressPaymentsStats(repository.Context(), &omsv1.GetAddressPaymentsStatsRequest{
			Address: []string{userInfo[id].Address},
			Year:    int32(time.Now().Year()),
			Month:   int32(time.Now().Month()),
		})
    // TODO: handle error via wrapped call in repository
		if err != nil {
			logger.Fatal(err)
		}
		userInfo[id].Activated = payments.Results[0].Paid

		// If vin is enabled in config we get carnumbers attached to address
		if d.config.Vin {
			res, err := d.repository.Address.GetAddressMetadata(
				repository.Context(),
				&omsv1.GetAddressMetadataRequest{
					Address: userInfo[id].Address,
					Key:     nil,
				},
			)
      // TODO: handle error via wrapped call in repository
			if err != nil {
				logger.Fatal(err)
			}
			userInfo[id].CarNumbers = res.Metas["car-numbers"].Values
		}

		if !userInfo[id].Activated {
			ctx.SetMessageText("identification/address-not-activated").Reply()
		}
	}

	userInfo[id].Identified = false

	_, err = d.repository.User.CreateUser(repository.Context(), userInfo[id])
	if err != nil {
		logger.Error(err)
		ctx.SetMessageText("identification/error").Reply()
	} else {
		user, _ := d.repository.User.GetUser(repository.Context(), &omsv1.GetUserRequest{
			Id:       &id,
			Username: nil,
		})
		d.repository.Event.Emit(repository.UserCreated, user)
		ctx.SetMessageText("identification/address-success").Reply()
	}
	delete(userInfo, id)
	return "", 0, true
}
