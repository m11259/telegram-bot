package domain

import (
	"gitlab.com/m11259/telegram-bot/cache"
	"gitlab.com/m11259/telegram-bot/config"
	"gitlab.com/m11259/telegram-bot/repository"

	"regexp"

	"gitlab.com/m11259/telegram-bot/handler"
)

type Domain struct {
	handler    *handler.Handler
	repository *repository.Services
	config     *config.Config
	sigma      *config.Sigma
	cache      *cache.Cache
}

type DomainOps struct {
	Handler    *handler.Handler
	Repository *repository.Services
	Config     *config.Config
	Sigma      *config.Sigma
	Cache      *cache.Cache
}

func New(ops DomainOps) *Domain {
	return &Domain{
		handler:    ops.Handler,
		repository: ops.Repository,
		config:     ops.Config,
		sigma:      ops.Sigma,
		cache:      ops.Cache,
	}
}

// Configure connects all defined by config
func (d *Domain) Configure(c *config.Config) {
	d.handler.F(d.Start).
    Update(handler.UpdateText).
    Regexp(regexp.MustCompile("/start"))

	if c.Components.Identification {
		d.ConnectIdentification()
		d.ConnectNotifications()
		d.ConnectUserInformation()
	}
	if c.Components.ManageRequests {
		d.ConnectManageRequests()
	}
	if c.Components.Request {
		d.ConnectRequest()
	}
	if c.Components.Payment {
		d.ConnectPayment()
	}
}

// ConnectIdentification links all handling functions about
// identification with handler
func (d *Domain) ConnectIdentification() {
	d.handler.F(d.identificationStart).
		Update(handler.UpdateText).
    Update(handler.UpdateCallback).
		Regexp(regexp.MustCompile("(/identify|identification-start)")).
		Priority(100)

	d.handler.F(d.getName).
		Update(handler.UpdateText).
		State("identification", 1).
		Priority(10)

	d.handler.F(d.getContact).
		Update(handler.UpdateText).
		Update(handler.UpdateContact).
		State("identification", 2).
		Priority(10)

	d.handler.F(d.getAddress).
		Update(handler.UpdateText).
		State("identification", 3).
		Priority(10)
}

func (d *Domain) ConnectPayment() {
	d.handler.F(d.getPayment).
		Update(handler.UpdateText).
		Regexp(regexp.MustCompile("/pay")).
		Priority(10)

	// d.handler.F(d.paymentDetails).
	// 	Update(handler.UpdateText).
	// 	Regexp(regexp.MustCompile("/pay"))

	// d.handler.F(d.sendPaymentInvoice).
	//     Update(handler.UpdateCallback).
	//     Regexp(regexp.MustCompile("pay"))
}

func (d *Domain) ConnectRequest() {
	d.handler.F(d.processRequestIntro).
		Update(handler.UpdateText).
		Update(handler.UpdateCallback).
		Regexp(regexp.MustCompile("^(/request|request-start)$")).
		Priority(100)

	d.handler.F(d.processRequestText).
		Update(handler.UpdateText).
		Regexp(regexp.MustCompile("^([^ /])")).
		Priority(0)

	d.handler.F(d.getRequestText).
		Update(handler.UpdateText).
		State("request", 1).
		Priority(10)

	d.handler.F(d.processUserAnswer).
		Update(handler.UpdateCallback).
		// State("request", 2).
		Priority(10)

  d.handler.F(d.confirmRequestCanceledCarnumber).
    Update(handler.UpdateCallback).
    State("request", 3).
    Regexp(regexp.MustCompile("^carnumber-cancel$")).
    Priority(5)

	d.handler.F(d.processUserCarnumber).
		Update(handler.UpdateText).
		State("request", 3).
		Priority(10)
}

func (d *Domain) ConnectUserInformation() {
	d.handler.F(d.getProfile).
		Update(handler.UpdateText).
		Regexp(regexp.MustCompile("/profile")).
		Priority(10)

	d.handler.F(d.unpaidMonths).
		Update(handler.UpdateText).
		Regexp(regexp.MustCompile("/unpaid")).
		Priority(10)

	d.handler.F(d.initChangeLang).
		Update(handler.UpdateText).
		Update(handler.UpdateCallback).
		Regexp(regexp.MustCompile("^(/lang|switch-lang)$")).
		Priority(20)

	d.handler.F(d.changeLang).
		Update(handler.UpdateCallback).
		Regexp(regexp.MustCompile("^switch-to-[a-z]+$")).
		Priority(50)

// I'm the Scatman
// Ski-bi dibby dib yo da dub dub
// Yo da dub dub
// Ski-bi dibby dib yo da dub dub
// Yo da dub dub
// (I'm the Scatman)
// Ski-bi dibby dib yo da dub dub
// Yo da dub dub
// Ski-bi dibby dib yo da dub dub
// Yo da dub dub
// Ba-da-ba-da-ba-be bop bop bodda bope
// Bop ba bodda bope
// Be bop ba bodda bope
// Bop ba bodda
// Ba-da-ba-da-ba-be bop ba bodda bope
// Bop ba bodda bope
// Be bop ba bodda bope
// Bop ba bodda bope
// Everybody stutters one way or the other
// So check out my message to you
// As a matter of fact, a-don't let nothin' hold you back
// If the Scatman can do it, so can you
// Everybody's sayin' that the Scatman stutters
// But doesn't ever stutter when he sings
// But what you don't know, I'm gonna tell you right now
// That the stutter and the scat is the same thing
// Yo, I'm the Scatman
// Where's the Scatman?
// I'm the Scatman
// Why should we be pleasin' any politician heathens
// Who would try to change the seasons if they could?
// The state of the condition insults my intuition
// And it only makes me crazy and a heart like wood
// Everybody stutters one way or the other
// So check out my message to you
// As a matter of fact, don't let nothin' hold you back
// If the Scatman can do it, brother, so can you
// I'm the Scatman
// Ba-da-ba-da-ba-be bop bop bodda bope
// Bop ba bodda bope
// Be bop ba bodda bope
// Bop ba bodda
// Ba-da-ba-da-ba-be bop ba bodda bope
// Bop ba bodda bope
// Be bop ba bodda bope
// Bop ba bodda bope
// Ski-bi dibby dib yo da dub dub
// Yo da dub dub
// Ski-bi dibby dib yo da dub dub
// Yo da dub dub
// Ski-bi dibby dib yo da dub dub
// Yo da dub dub
// Ski-bi dibby dib yo da dub dub
// Yo da dub dub
// Everybody stutters one way or the other
// So check out my message to you
// As a matter of fact, a-don't let nothin' hold you back
// If the Scatman can do it, so can you
// I hear you all ask 'bout the meaning of scat
// Well, I'm the professor, and all I can tell you is
// While you're still sleepin', the saints are still weepin' 'cause
// Things you call dead haven't yet had the chance to be born
// I'm the Scatman
// Ba-da-ba-da-ba-be bop bop bodda bope
// Bop ba bodda bope
// Be bop ba bodda bope
// Bop ba bodda
// Ba-da-ba-da-ba-be bop ba bodda bope
// Bop ba bodda bope
// Be bop ba bodda bope
// Bop ba bodda bope
// Ski-bi dibby dib yo da dub dub
// Yo da dub dub
// Ski-bi dibby dib yo da dub dub
// Yo da dub dub
// Ski-bi dibby dib yo da dub dub
// Yo da dub dub
// Ski-bi dibby dib yo da dub dub
// (Yo da dub dub) yeah, I'm the Scatman
// Where's the Scatman?
// I'm the Scatman, repeat after me
// It's a scoobie oobie doobie, scoobie doobie melody
// I'm the Scatman, sing along with me
// It's a scoobie oobie doobie, scoobie doobie melody
// Yeah, I'm the Scatman (I'm the Scatman)
// Bop ba bodda bope
// Be bop ba bodda bope
// Bop ba bodda
// I'm the Scatman (I'm the Scatman)
// Bop ba bodda bope
// Be bop ba bodda bope
// Ba-da-ba-da-ba-be bop bop bodda bope
// Bop ba bodda bope
// Be bop ba bodda bope
// Bop ba bodda
// Ba-da-ba-da-ba-be bop ba bodda bope (yeah, I'm the Scatman)
// Bop ba bodda bope (sing along with me)
// Be bop ba bodda bope (it's a scoobie oobie doobie, scoobie doobie melody)
}

func (d *Domain) ConnectManageRequests() {
	d.handler.F(d.viewRequests).
		Update(handler.UpdateText).
		Update(handler.UpdateCallback).
		Regexp(regexp.MustCompile("(/view_requests|view-requests)")).
		Priority(100)

	d.handler.F(d.viewRequestsNextPage).
		Update(handler.UpdateCallback).
		Regexp(regexp.MustCompile("^reqmanage-next-page$")).
		Priority(20)

	d.handler.F(d.viewRequestsPrevPage).
		Update(handler.UpdateCallback).
		Regexp(regexp.MustCompile("^reqmanage-prev-page$")).
		Priority(20)

	d.handler.F(d.cancelRequest).
		Update(handler.UpdateCallback).
		Regexp(regexp.MustCompile("^cancel-request-[0-9]+$")).
		Priority(20)
}

func (d *Domain) ConnectNotifications() {
	d.handler.F(d.onReqAccepted).
		Update(handler.EventRequestAccepted)

	d.handler.F(d.onReqDenied).
		Update(handler.EventRequestDenied)

	d.handler.F(d.onReqUnaccepted).
		Update(handler.EventRequestUnaccepted)

	d.handler.F(d.onReqUndenied).
		Update(handler.EventRequestUndenied)

	d.handler.F(d.onUserActivated).
		Update(handler.EventUserActivated)

	d.handler.F(d.onUserDenied).
		Update(handler.EventUserDenied)

	d.handler.F(d.onUserApproved).
		Update(handler.EventUserApproved)

	d.handler.F(d.onUserDeactivated).
		Update(handler.EventUserDectivated)

	d.handler.F(d.onReqUpdated).
		Update(handler.EventRequestUpdated)
}
