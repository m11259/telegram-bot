package config

import (
	"strings"

	"gitlab.com/m11259/telegram-bot/logger"
	"gitlab.com/m11259/telegram-bot/repository"

	omsv1 "go.buf.build/grpc/go/aipyth/ongate-oms/oms/v1"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Sigma struct {
	config      *SigmaConfig
	textStorage omsv1.TelegramTextStorageServiceClient
}

func NewSigma(config *SigmaConfig, textStorage omsv1.TelegramTextStorageServiceClient) *Sigma {
	return &Sigma{
		config:      config,
		textStorage: textStorage,
	}
}

func (s *Sigma) DefaultLanguage() string {
	return "uk"
}

func (s *Sigma) IsLangAvailable(lang string) bool {
	for _, l := range s.config.SupportedLangs() {
		if l == lang {
			return true
		}
	}
	return false
}

// IsPopulated returns boolean value indicating whether redis was populated
func (s *Sigma) IsPopulated() bool {
  keys := make([]string, 0, len(s.config.Keys))
  for k := range s.config.Keys {
    keys = append(keys, k)
  }
  if len(keys) == 0 { return false }
	_, err := s.textStorage.GetKey(repository.Context(), &omsv1.GetKeyRequest{
		Key: keys[0],
	})
	if stat, ok := status.FromError(err); ok {
		if stat.Code() == codes.NotFound {
			return false
		}
	} else if err != nil {
		logger.Error("[Sigma:isPopulated]", err)
	}
	return true
}

// Populate writes data from sigmaConfig to oms text storage
func (s *Sigma) Populate() error {
	keys := []*omsv1.TextKey{}
	for k, v := range s.config.Keys {
		keys = append(keys, v.ToProto(k))
	}
	_, err := s.textStorage.Populate(repository.Context(), &omsv1.PopulateRequest{Keys: keys})
	if err != nil {
		logger.Error("[Sigma:Populate]", err)
	}
	return nil
}

// get value from redis
func (s *Sigma) Get(lang string, args string) (string, error) {
	key := strings.ReplaceAll(args, "/", ":")
	value, err := s.textStorage.GetKey(repository.Context(), &omsv1.GetKeyRequest{
		Key: key,
	})
	if err != nil {
		logger.Error("[Sigma]", err)
		return "", err
	}
	return value.Template[lang], nil
}

func parseKeyboard(rows []*omsv1.KeyboardRow, lang string) [][]map[string]string {
	obj := [][]map[string]string{}
	for _, row := range rows {
		resultRows := []map[string]string{}
		for _, butt := range row.Buttons {
			resultButtons := map[string]string{}
			resultButtons["name"] = butt.Text[lang]
			if butt.Data != nil {
				resultButtons["data"] = *butt.Data
			}
			if butt.Contact != nil {
				if *butt.Contact {
					resultButtons["contact"] = "yes"
				}
			}
			resultRows = append(resultRows, resultButtons)
		}
		obj = append(obj, resultRows)
	}
	return obj
}

func (s *Sigma) Keyboard(lang, name string) ([][]map[string]string, error) {
	key := strings.ReplaceAll(name, "/", ":")
	value, err := s.textStorage.GetKey(repository.Context(), &omsv1.GetKeyRequest{
		Key: key,
	})
	if err != nil {
		logger.Error("[Sigma] getting keyboard with path", key, "gives error", err)
		return nil, err
	}
	return parseKeyboard(value.Keyboard.Rows, lang), nil
}
