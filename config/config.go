package config

import (
	"io/ioutil"

	"encoding/json"
)

type Config struct {
	Debug            bool `json:"debug"`
	TGBotApiDebug    bool `json:"tg-debug"`
	PaymentThreshold int  `json:"payment-threshold"`
	Vin              bool `json:"vin"`
	Components       struct {
		Identification bool `json:"identification"`
		Payment        bool `json:"payment"`
		ManageRequests bool `json:"manage-requests"`
		Request        bool `json:"request"`
	} `json:"components"`
	Sigma struct {
		UseRedis        bool `json:"use-redis"`
		PopulateOnStart bool `json:"populate-on-start"`
	} `json:"sigma"`
}

func GetConfig() (*Config, error) {
	contents, err := ioutil.ReadFile("./config.json")
	if err != nil {
		return nil, err
	}

	conf := new(Config)
	err = json.Unmarshal(contents, conf)
	if err != nil {
		return nil, err
	}
	return conf, err
}
