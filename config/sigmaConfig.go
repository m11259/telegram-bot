package config

import (
	"io/ioutil"

	omsv1 "go.buf.build/grpc/go/aipyth/ongate-oms/oms/v1"
	"gopkg.in/yaml.v2"
)

var Langs = []string{"uk", "en", "ru"}

type TextKey struct {
	Section     string             `yarn:"section"`
	Editable    bool               `yarn:"editable"`
	Template    map[string]string  `yarn:"template"`
	Description string             `yarn:"description"`
	Vars        map[string]string  `yarn:"vars"`
	Keyboard    [][]KeyboardButton `yarn:"keyboard"`
}

type KeyboardButton struct {
	Text    map[string]string `yarn:"text"`
	Data    *string           `yarn:"data"`
	Contact *bool             `yarn:"contact"`
}

type SigmaConfig struct {
	Keys map[string]TextKey
}

func GetSigmaConfig() (*SigmaConfig, error) {
	contents, err := ioutil.ReadFile("./sigma-config.yml")
	if err != nil {
		return nil, err
	}

	conf := new(SigmaConfig)
	err = yaml.Unmarshal(contents, &conf.Keys)
	if err != nil {
		return nil, err
	}
	return conf, nil
}

func (s *SigmaConfig) SupportedLangs() []string {
	return Langs
}

func (b *KeyboardButton) toProto() *omsv1.KeyboardButton {
	return &omsv1.KeyboardButton{
		Text:    b.Text,
		Data:    b.Data,
		Contact: b.Contact,
	}
}

func (key *TextKey) ToProto(k string) *omsv1.TextKey {
	rows := []*omsv1.KeyboardRow{}
	for _, row := range key.Keyboard {
		buttons := []*omsv1.KeyboardButton{}
		for _, button := range row {
			buttons = append(buttons, button.toProto())
		}
		rows = append(rows, &omsv1.KeyboardRow{Buttons: buttons})
	}
	return &omsv1.TextKey{
		Key:         k,
		Section:     key.Section,
		Editable:    key.Editable,
		Template:    key.Template,
		Description: key.Description,
		Vars:        key.Vars,
		Keyboard:    &omsv1.Keyboard{Rows: rows},
	}
}
