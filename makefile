# the image is contains in gitlab container registry
image_name := registry.gitlab.com/m11259/telegram-bot
sources := main.go ./**/*.go

.PHONY: all build build-image push-image

all: build-image push-image

build-image: $(sources)
	docker build -t $(image_name) .

push-image:
	docker push $(image_name)

build:
	go build -o main .
