package handler

import (
	"encoding/json"
	"reflect"
	"regexp"
	"runtime"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

	"gitlab.com/m11259/telegram-bot/logger"
)

func New() *Handler {
	hdl := new(Handler)

	hdl.residents = make([]HandlingFunc, 0)
	hdl.endpoints = make(map[*Fit]HandlingFunc)
	hdl.states = make(map[int64]*State)
	return hdl
}

func (h *Handler) OnAll(f HandlingFunc) {
	h.residents = append(h.residents, f)
}

func (h *Handler) F(f HandlingFunc) *HandlerRes {
	fit := &Fit{
		UpdateTypes: make([]UpdateType, 0),
		Regexp:      nil,
		State: &State{
			Name: "",
			Step: 0,
		},
	}
	h.endpoints[fit] = f
	return &HandlerRes{
		handler: h,
		fit:     fit,
		f:       f,
	}
}

func (h *HandlerRes) Update(updateType UpdateType) *HandlerRes {
	h.fit.UpdateTypes = append(h.fit.UpdateTypes, updateType)
	return h
}

func (h *HandlerRes) Regexp(rgx *regexp.Regexp) *HandlerRes {
	h.fit.Regexp = rgx
	return h
}

func (h *HandlerRes) State(name string, step int) *HandlerRes {
	h.fit.State.Name = name
	h.fit.State.Step = step
	return h
}

func (h *HandlerRes) Priority(priority int) *HandlerRes {
	h.fit.Priority = priority
	return h
}

func (h *Handler) ProcessUpdate(bot *tgbotapi.BotAPI, update Update, ctx *Ctx) {
	for _, rsd := range h.residents {
		_, _, _ = rsd(ctx)
	}

	chatId := ctx.ID()
	// set empty state if none is set
	if _, ok := h.states[chatId]; !ok {
		h.states[chatId] = &State{
			Name: "",
			Step: 0,
		}
	}

	state := h.states[chatId]
	ctx.State = *state

	logger.Debugf("current state %+v\n", state)

	execHandler := func(handler HandlingFunc, state *State) {
		logExecutionOn(handler, update)
		start := time.Now()
		newName, step, reset := handler(ctx)
		t := time.Since(start)
		logger.Infof("%s executed in %v\n", getFunctionName(handler), t)
		if reset && newName == "" && step != 0 {
			return
		}
		state.Name = newName
		if reset {
			state.Step = 0
		}
		state.Step += step
	}

	candidates := make(map[int][]HandlingFunc)
	addCandidate := func(priority int, f HandlingFunc) {
		_, ok := candidates[priority]
		if !ok {
			candidates[priority] = make([]HandlingFunc, 0)
		}
		candidates[priority] = append(candidates[priority], f)
	}

	for f, hf := range h.endpoints {
    // logger.Debugf("fit for state %+v\n", map[string]interface{}{
    //   "fit": *f,
    //   "update": update,
    //   "state": *state,
    //   "fitsWithState": f.FitsWithState(update, state),
    //   "fitsOnNullState": f.Fits(update) && f.State.Name == "",
    // })
		if f.FitsWithState(update, state) || (f.Fits(update) && f.State.Name == "") {
			addCandidate(f.Priority, hf)
		}
	}

  logger.Debug("CANDIDATES\n")
  for k, v := range candidates {
    for _, hf := range v {
      logger.Debugf("%d -> %s\n", k, getFunctionName(hf))
    }
  }

  // get maximum priority written
	maxPriority := 0
	for k := range candidates {
		if k > maxPriority { maxPriority = k }
	}

  // pick the candidate with this maximum priority
	f, ok := candidates[maxPriority]
  // if no candidate just print the information into the console
	if !ok {
		u, _ := json.Marshal(update)
		logger.Infof("no matching handler found for update %s\n", u)
		return
	}

	logger.Debugf("MAX PRIORITY SELECTED %d\n", maxPriority)

	for _, hf := range f {
		execHandler(hf, state)
	}
	return
}

func logExecutionOn(handler HandlingFunc, update Update) {
	u, _ := json.Marshal(update)
	logger.Printf("Executing handler %s on update %s\n", getFunctionName(handler), u)
}

func getFunctionName(i interface{}) string {
	return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
}
