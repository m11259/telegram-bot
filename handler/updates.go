package handler

import (
	"gitlab.com/m11259/telegram-bot/repository"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type UpdateType string

const UpdateNil = ""
const UpdateText = "text"
const UpdateEdited = "edited"
const UpdateInlineQuery = "inline-query"
const UpdateInlineChosen = "inline-chosen"
const UpdateCallback = "callback"
const UpdateShippingQuery = "ship-query"
const UpdatePreCheckoutQuery = "pre-check-query"

const UpdateAudio = "audio"
const UpdateDocument = "document"
const UpdatePhoto = "photo"
const UpdateVideo = "video"
const UpdateVoice = "voice"
const UpdateContact = "contact"
const UpdateLocation = "location"
const UpdateInvoice = "invoice"
const UpdateSuccessfulPayment = "success-payment"
const UpdatePassport = "passport"
const UpdatePostgres = "postgres"

const EventUserCreated = "user/create"
const EventUserApproved = "user/identify"
const EventUserDenied = "user/deny"
const EventUserActivated = "user/activate"
const EventUserDectivated = "user/deactivate"
const EventRequestAccepted = "request/accept"
const EventRequestUpdated = "request/update"
const EventRequestDenied = "request/deny"
const EventRequestUnaccepted = "request/unaccept"
const EventRequestUndenied = "request/undeny"
const EventRequestCreated = "request/create"
const EventRequestCancel = "request/cancel"

type Update struct {
	*tgbotapi.Update
	EventUpdate *repository.EventUpdate
}

func DetermineUpdateType(upd Update) UpdateType {
	switch {
	case upd.EventUpdate != nil:
		return DetermineEventType(repository.Event(upd.EventUpdate.Event))
	case upd.InlineQuery != nil:
		return UpdateInlineQuery
	case upd.ChosenInlineResult != nil:
		return UpdateInlineChosen
	case upd.CallbackQuery != nil:
		return UpdateCallback
	case upd.ShippingQuery != nil:
		return UpdateShippingQuery
	case upd.PreCheckoutQuery != nil:
		return UpdatePreCheckoutQuery
	case upd.EditedMessage != nil:
		return DetermineMessageUpdateType(*upd.EditedMessage)
	case upd.Message != nil:
		return DetermineMessageUpdateType(*upd.Message)
	default:
		return UpdateNil
	}
}

func DetermineMessageUpdateType(msg tgbotapi.Message) UpdateType {
	switch {
	case msg.Audio != nil:
		return UpdateAudio
	case msg.Document != nil:
		return UpdateDocument
	case msg.Photo != nil:
		return UpdatePhoto
	case msg.Video != nil:
		return UpdateVideo
	case msg.Voice != nil:
		return UpdateVoice
	case msg.Contact != nil:
		return UpdateContact
	case msg.Location != nil:
		return UpdateLocation
	case msg.Invoice != nil:
		return UpdateInvoice
	case msg.SuccessfulPayment != nil:
		return UpdateSuccessfulPayment
	case msg.PassportData != nil:
		return UpdatePassport
	case msg.Text != "":
		return UpdateText
	default:
		return UpdateNil
	}
}

func DetermineEventType(event repository.Event) UpdateType {
	switch event {
	case repository.UserApproved:
		return EventUserApproved
	case repository.UserActivated:
		return EventUserActivated
	case repository.UserDectivated:
		return EventUserDectivated
	case repository.UserDenied:
		return EventUserDenied
	case repository.RequestAccepted:
		return EventRequestAccepted
	case repository.RequestDenied:
		return EventRequestDenied
	case repository.RequestUndenied:
		return EventRequestUndenied
	case repository.RequestUnaccepted:
		return EventRequestUnaccepted
	case repository.RequestUpdated:
		return EventRequestUpdated
	default:
		return UpdateNil
	}
}
