package handler

import tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

func (c *Ctx) SetPayment() {
	prices := &[]tgbotapi.LabeledPrice{
		{Label: "UAH", Amount: 200},
	}
	invoice := tgbotapi.NewInvoice(c.ID(), "title", "desc",
		"payment_payload", "1661751239:TEST:339585806",
		"startparam", "UAH", prices)
	c.config.InvoiceConfig = &invoice
}
