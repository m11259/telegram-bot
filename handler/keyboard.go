package handler

import (
	"log"

	"gitlab.com/m11259/telegram-bot/logger"
	"gitlab.com/m11259/telegram-bot/wrapper"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

var inlineKeywords = []string{"url", "data", "switch_inline_query", "switch_inline_query_current_chat", "callback_game", "pay"}
var replyKeywords = []string{"contact", "location"}

type AppendButtonParams struct {
	id   int                    // Path in sigma config for button.
	args map[string]interface{} // Args for wrapper
}

func (ctx *Ctx) NewButtonParams(id int, args ...map[string]interface{}) AppendButtonParams {
	if len(args) == 0 {
		return AppendButtonParams{
			id:   id,
			args: nil,
		}
	}
	log.Printf("ARGS: %+v", args[0])
	return AppendButtonParams{
		id:   id,
		args: args[0],
	}
}

// detKeyboardType return keyboard type inline|reply
func detKeyboardType(keyboard [][]map[string]string) string {
	if len(keyboard) == 0 || len(keyboard[0]) == 0 {
		return ""
	}
	for _, kw := range inlineKeywords {
		if _, ok := keyboard[0][0][kw]; ok {
			return "inline"
		}
	}
	for _, kw := range replyKeywords {
		if _, ok := keyboard[0][0][kw]; ok {
			return "reply"
		}
	}
	return ""
}

// SetKeyboard adds keyboard from sigma-config to the message
func (ctx *Ctx) SetKeyboard(path string) *Ctx {
	kb, err := ctx.sigma.Keyboard(ctx.Lang(), path)
	if err != nil {
		ctx.HandleError(err)
		return ctx
	}
	switch detKeyboardType(kb) {
	case "inline":
		ctx.config.MessageConfig.ReplyMarkup = ctx.buildInlineKeyboard(kb)
		return ctx
	case "reply":
		ctx.config.MessageConfig.ReplyMarkup = ctx.buildReplyKeyboard(kb)
		logger.Infof("%v", ctx.config.MessageConfig.ReplyMarkup)
		return ctx
	}
	return ctx
}

// SetEditKeyboard edits existing keyboard in last message from sigma-config
func (ctx *Ctx) SetEditKeyboard(path string) *Ctx {
	kb, err := ctx.sigma.Keyboard(ctx.Lang(), path)
	if err != nil {
		ctx.HandleError(err)
		return ctx
	}
	switch detKeyboardType(kb) {
	case "inline":
		new_kb := ctx.buildInlineKeyboard(kb)
		ctx.config.EditMessageReplyMarkupConfig = &tgbotapi.EditMessageReplyMarkupConfig{
			BaseEdit: tgbotapi.BaseEdit{
				ChatID:      ctx.ID(),
				MessageID:   ctx.messageID(),
				ReplyMarkup: &new_kb,
			},
		}
		return ctx
	}
	return ctx
}

// AppendInlineButtonRow appends row of buttons to existing keyboard to build new or edit existing keyboard using array of paths in sigma
func (ctx *Ctx) AppendInlineButtonRow(edit bool, path string, params ...AppendButtonParams) *Ctx {
	var kb [][]tgbotapi.InlineKeyboardButton
	if edit && ctx.config.EditMessageReplyMarkupConfig != nil {
		kb = ctx.config.EditMessageReplyMarkupConfig.ReplyMarkup.InlineKeyboard
	} else if ctx.config.MessageConfig != nil {
		if ctx.config.MessageConfig.ReplyMarkup != nil {
			kb = ctx.config.MessageConfig.ReplyMarkup.(tgbotapi.InlineKeyboardMarkup).InlineKeyboard
		}
	}
	buttonRow := make([]tgbotapi.InlineKeyboardButton, len(params))
	buttons, err := ctx.sigma.Keyboard(ctx.Lang(), path)
	if err != nil {
		ctx.HandleError(err)
		return ctx
	}
  logger.Debug(buttons)
	for i := range params {
		text, data := "", ""
    logger.Debug(buttons[params[i].id])
    logger.Debug("item", buttons[params[i].id][0])
		if res, ok := buttons[params[i].id][0]["name"]; ok {
			text = res
		} else {
			logger.Errorf("[Sigma]: buttons broken structure: %s", path)
		}
		if res, ok := buttons[params[i].id][0]["data"]; ok {
			data = res
		} else {
			logger.Errorf("[Sigma]: buttons broken structure: %s", path)
		}
		buttonRow[i] = buildInlineButton(map[string]string{
			"name": wrapper.Wrap(text, params[i].args),
			"data": wrapper.Wrap(data, params[i].args),
		})
	}
	kb = append(kb, buttonRow)
	if edit {
		edited_kb := tgbotapi.NewInlineKeyboardMarkup(kb...)
		ctx.config.EditMessageReplyMarkupConfig = &tgbotapi.EditMessageReplyMarkupConfig{
			BaseEdit: tgbotapi.BaseEdit{
				ChatID:      ctx.ID(),
				MessageID:   ctx.messageID(),
				ReplyMarkup: &edited_kb,
			},
		}
	} else {
		ctx.config.MessageConfig.ReplyMarkup = tgbotapi.NewInlineKeyboardMarkup(kb...)
	}
	return ctx
}

// detInlineButtonType returns button (in a form of map) type from inlineKeywords
func detInlineButtonType(mapping map[string]string) string {
	for _, kw := range inlineKeywords {
		if _, ok := mapping[kw]; ok {
			return kw
		}
	}
	return ""
}

func buildInlineButton(mapping map[string]string) tgbotapi.InlineKeyboardButton {
	switch detInlineButtonType(mapping) {
	case "url":
		return tgbotapi.NewInlineKeyboardButtonURL(mapping["name"], mapping["url"])
	case "data":
		return tgbotapi.NewInlineKeyboardButtonData(mapping["name"], mapping["data"])
	case "switch_inline_query":
		return tgbotapi.NewInlineKeyboardButtonSwitch(mapping["name"], mapping["switch_inline_query"])
	case "switch_inline_query_current_chat":
		x := tgbotapi.NewInlineKeyboardButtonSwitch(mapping["name"], mapping["switch_inline_query_current_chat"])
		x.SwitchInlineQueryCurrentChat = x.SwitchInlineQuery
		x.SwitchInlineQuery = nil
		return x
	case "pay":
		return tgbotapi.InlineKeyboardButton{
			Text: mapping["name"],
			Pay:  true,
		}
	}
	return tgbotapi.InlineKeyboardButton{}
}

// setInlineKeyboard builds Inline Keyboard from matrix of mappings
func (ctx *Ctx) buildInlineKeyboard(kb [][]map[string]string) tgbotapi.InlineKeyboardMarkup {
	rows := make([][]tgbotapi.InlineKeyboardButton, len(kb))
	for i := range kb {
		row := make([]tgbotapi.InlineKeyboardButton, len(kb[i]))
		for j := range kb[i] {
			row[j] = buildInlineButton(kb[i][j])
		}
		rows[i] = row
	}
	return tgbotapi.NewInlineKeyboardMarkup(rows...)
}

func buildReplyButton(mapping map[string]string) tgbotapi.KeyboardButton {
	_, contact := mapping["contact"]
	return tgbotapi.KeyboardButton{
		Text:           mapping["name"],
		RequestContact: contact,
		// RequestLocation: mapping["location"] == "yes" || mapping["location"] == "true",
	}
}

// setReplyKeyboard builds reply keyboard from matrix of mappings
func (ctx *Ctx) buildReplyKeyboard(kb [][]map[string]string) tgbotapi.ReplyKeyboardMarkup {
	rows := make([][]tgbotapi.KeyboardButton, len(kb))
	for i := range kb {
		row := make([]tgbotapi.KeyboardButton, len(kb[i]))
		for j := range kb[i] {
			row[j] = buildReplyButton(kb[i][j])
		}
		rows[i] = row
	}
	// ctx.config.MessageConfig.ReplyMarkup = tgbotapi.NewReplyKeyboard(rows...)
	return tgbotapi.NewReplyKeyboard(rows...)

}

// OneTimeKeyboard makes ReplyKeyboardMarkup one time usable
func (ctx *Ctx) OneTimeKeyboard() *Ctx {
	keyboard := ctx.config.MessageConfig.ReplyMarkup.(tgbotapi.ReplyKeyboardMarkup)
	keyboard.OneTimeKeyboard = true
	ctx.config.MessageConfig.ReplyMarkup = keyboard
	return ctx
}

// HideKeyboard hides ReplyKeyboardMarkup
func (ctx *Ctx) HideKeyboard() *Ctx {
	ctx.config.MessageConfig.ReplyMarkup = tgbotapi.NewRemoveKeyboard(false)
	return ctx
}

func (ctx *Ctx) HideInlineKeyboard() *Ctx {
	kb := tgbotapi.InlineKeyboardMarkup{
		InlineKeyboard: make([][]tgbotapi.InlineKeyboardButton, 0),
	}
  if ctx.config.EditMessageTextConfig == nil {
    ctx.config.EditMessageTextConfig = &tgbotapi.EditMessageTextConfig{}
  }
  ctx.config.EditMessageTextConfig.BaseEdit.ReplyMarkup = &kb
	return ctx
}
