package handler

import (
	"regexp"
)

// HandlingFunc means function that handles the update. It should take
// bot and update as arguments and return the state to switch to,
// the step to be added to current chat's state step and boolean representing
// the need of reseting the step. (Firstly it resets -> then thet step is added)
type HandlingFunc func(ctx *Ctx) (string, int, bool)

type State struct {
	Name string
	Step int
}

type Fit struct {
	UpdateTypes []UpdateType
	Regexp      *regexp.Regexp
	State       *State
	Priority    int
}

// Handler is an update dispatcher that dispatches update to all functions
// that fits that update
type Handler struct {
	residents []HandlingFunc
	endpoints map[*Fit]HandlingFunc
	states    map[int64]*State
}

type HandlerRes struct {
	handler *Handler
	fit     *Fit
	f       HandlingFunc
}
