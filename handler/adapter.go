package handler

import (
	"encoding/json"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

	"gitlab.com/m11259/telegram-bot/cache"
	"gitlab.com/m11259/telegram-bot/config"
	"gitlab.com/m11259/telegram-bot/logger"
	"gitlab.com/m11259/telegram-bot/repository"
	"gitlab.com/m11259/telegram-bot/wrapper"
)

type ContextConfig struct {
	MessageConfig                *tgbotapi.MessageConfig
	EditMessageTextConfig        *tgbotapi.EditMessageTextConfig
	EditMessageReplyMarkupConfig *tgbotapi.EditMessageReplyMarkupConfig
	InvoiceConfig                *tgbotapi.InvoiceConfig
}

type Ctx struct {
	bot    *tgbotapi.BotAPI
	update Update

	cache *cache.Cache
	sigma *config.Sigma
	State State

	config ContextConfig
	send   tgbotapi.Chattable
}

func NewCtx(bot *tgbotapi.BotAPI, update Update, cache *cache.Cache, sgm *config.Sigma) *Ctx {
	return &Ctx{
		bot:    bot,
		update: update,
		cache:  cache,
		sigma:  sgm,
	}
}

func (ctx *Ctx) ID() int64 {
	if ctx.update.EventUpdate != nil {
		return ctx.update.EventUpdate.Id
	}
	return int64(ctx.From().ID)
}

func (ctx *Ctx) From() *tgbotapi.User {
	if ctx.update.EventUpdate != nil {
		return nil
	}
	switch {
	case ctx.update.Message != nil:
		return ctx.update.Message.From
	case ctx.update.EditedMessage != nil:
		return ctx.update.EditedMessage.From
	case ctx.update.InlineQuery != nil:
		return ctx.update.InlineQuery.From
	case ctx.update.ChosenInlineResult != nil:
		return ctx.update.ChosenInlineResult.From
	case ctx.update.CallbackQuery != nil:
		return ctx.update.CallbackQuery.From
	case ctx.update.ShippingQuery != nil:
		return ctx.update.ShippingQuery.From
	case ctx.update.PreCheckoutQuery != nil:
		return ctx.update.PreCheckoutQuery.From
	default:
		return nil
	}

}

func (ctx *Ctx) messageID() int {
	switch {
	case ctx.update.Update != nil && ctx.update.Update.Message != nil:
		return ctx.update.Update.Message.MessageID
	case ctx.update.Update != nil && ctx.update.Update.CallbackQuery != nil:
		return ctx.update.Update.CallbackQuery.Message.MessageID
	default:
		return 0
	}
}

func (ctx *Ctx) Text() string {
	switch {
	case ctx.update.Update != nil && ctx.update.Update.Message != nil:
		return ctx.update.Update.Message.Text
	case ctx.update.Update != nil && ctx.update.Update.CallbackQuery != nil:
		return ctx.update.Update.CallbackQuery.Message.Text
	default:
		return ""
	}
}

func (ctx *Ctx) Data() string {
	if ctx.update.EventUpdate != nil {
		return ""
	}
	switch {
	case ctx.update.Update.CallbackQuery != nil:
		return ctx.update.CallbackQuery.Data
	case ctx.update.Update.Message != nil:
		return ctx.update.Message.Text
	default:
		return ""
	}
}

func (ctx *Ctx) EventData() repository.EventData {
	if ctx.update.EventUpdate != nil {
		return ctx.update.EventUpdate.Data
	}
	return nil
}

func (ctx *Ctx) Username() string {
	if ctx.update.Message != nil {
		return ctx.update.Message.From.UserName
	} else {
		return ""
	}
}

func (ctx *Ctx) Telegramname() string {
	if ctx.update.Message != nil {
		return ctx.update.Message.From.FirstName
	} else {
		return ""
	}
}

func (ctx *Ctx) Contact() string {
	if ctx.update.Message.Contact != nil {
		return ctx.update.Message.Contact.PhoneNumber
	} else {
		return ctx.update.Message.Text
	}
}

func (ctx *Ctx) Lang() string {
	lang, err := ctx.cache.GetLang(ctx.ID())
	if err != nil || lang == "" {
		lang := ctx.From().LanguageCode
		if !ctx.sigma.IsLangAvailable(lang) {
			return ctx.sigma.DefaultLanguage()
		}
		return lang
	}
	return lang
}

func (ctx *Ctx) SetLang(code string) {
	ctx.cache.SetLang(ctx.ID(), code)
}

func (ctx *Ctx) SetMessageText(path string, args ...map[string]interface{}) *Ctx {
	text, err := ctx.sigma.Get(ctx.Lang(), path)
	if err != nil {
		ctx.HandleError(err)
		return ctx
	}
	if len(args) > 0 {
		text = wrapper.Wrap(text, args[0])
	}
	ctx.config.MessageConfig = &tgbotapi.MessageConfig{
		BaseChat: tgbotapi.BaseChat{
			ChatID:           ctx.ID(),
			ReplyToMessageID: 0,
		},
		Text:                  text,
		DisableWebPagePreview: false,
	}
	ctx.config.MessageConfig.ParseMode = "Markdown"
	return ctx
}

func (ctx *Ctx) SetEditMessageText(path string, args ...map[string]interface{}) *Ctx {
	text, err := ctx.sigma.Get(ctx.Lang(), path)
	if err != nil {
		ctx.HandleError(err)
		return ctx
	}
	if len(args) > 0 {
		text = wrapper.Wrap(text, args[0])
	}
	ctx.config.EditMessageTextConfig = &tgbotapi.EditMessageTextConfig{
		BaseEdit: tgbotapi.BaseEdit{
			ChatID:    ctx.ID(),
			MessageID: ctx.messageID(),
		},
		Text:                  text,
		DisableWebPagePreview: false,
		ParseMode:             ctx.GetParseMode(),
	}
	return ctx
}

func (ctx *Ctx) GetParseMode() string {
  return "Markdown"
}

func (ctx *Ctx) Reply() {
	var resp interface{}
	var err error
	if ctx.config.MessageConfig != nil {
		resp, err = ctx.bot.Send(ctx.config.MessageConfig)
		logReplyResponse(resp, err)
	}
	if ctx.config.EditMessageTextConfig != nil {
		resp, err = ctx.bot.Send(ctx.config.EditMessageTextConfig)
		logReplyResponse(resp, err)
	}
	if ctx.config.EditMessageReplyMarkupConfig != nil {
		resp, err = ctx.bot.Send(ctx.config.EditMessageReplyMarkupConfig)
		logReplyResponse(resp, err)
	}
	if ctx.update.Update != nil && ctx.update.CallbackQuery != nil {
		resp, err = ctx.bot.AnswerCallbackQuery(tgbotapi.NewCallback(ctx.update.CallbackQuery.ID, ""))
		logReplyResponse(resp, err)
	}
	if ctx.config.InvoiceConfig != nil {
		resp, err = ctx.bot.Send(ctx.config.InvoiceConfig)
		logReplyResponse(resp, err)
	}
}

func logReplyResponse(resp interface{}, err error) {
	rb, _ := json.Marshal(resp)
	if err != nil {
		logger.Errorf("Tgbotapi Send: %v\n%s", err, rb)
	} else {
		logger.Debugf("response: %s\n", rb)
	}
}

func (ctx *Ctx) HandleError(err error) {
	ctx.SetMessageText("notify/error")
	ctx.config.MessageConfig.ReplyMarkup = nil
	logger.Error(err)
	ctx.Reply()
}
