package handler

func (f *Fit) matchesRegexp(update Update) bool {
	updateT := DetermineUpdateType(update)
	if f.Regexp != nil {
		switch updateT {
		case UpdateText:
			return f.Regexp.MatchString(update.Message.Text)
		case UpdateCallback:
			return f.Regexp.MatchString(update.CallbackQuery.Data)
		case UpdateDocument, UpdatePhoto, UpdateVideo:
			return f.Regexp.MatchString(update.Message.Caption)
		default:
			return false
		}
	}
	return true
}

func (f *Fit) GetUpdateTypeToFit(update Update) UpdateType {
	updateT := DetermineUpdateType(update)
	for i, upd := range f.UpdateTypes {
		if updateT == upd {
			break
		}
		if i == len(f.UpdateTypes)-1 {
			return UpdateNil
		}
	}
	return updateT
}

func (f *Fit) Fits(update Update) bool {
	updateType := f.GetUpdateTypeToFit(update)
	if updateType == UpdateNil {
		return false
	}
	if !f.matchesRegexp(update) {
		return false
	}
	return true
}

func (f *Fit) FitsWithState(update Update, currentState *State) bool {
	if !f.Fits(update) {
		return false
	}
	if f.State.Name == currentState.Name && f.State.Step == currentState.Step {
		return true
	}
	return false
}
