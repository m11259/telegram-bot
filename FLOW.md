# Telegram Bot Conversation Flow

```mermaid
stateDiagram-v2
  [*] --> /start
  [*] --> processRequest : User just sends a text message

  /start --> identification
  /start --> inline_send_request : Start sending request by clicking on inline button

  state identification {
    [*] --> /identify
    [*] --> inline_identify : [inline] Start identificati
  }

  state processRequest {
    [*] --> requestConfirmation : bot accepts user's message
    [*] --> denyUserRequest : user is not activated or identified
    requestConfirmation --> addCarNumber : user clicks on a button to add a request
    addCarNumber --> requestConfirmation : valid car number
    addCarNumber --> reaskCarNumber : not valid car number
    reaskCarNumber --> requestConfirmation : valid car number
    requestConfirmation --> requestSent : user confirms action
    requestConfirmation --> requestNotSent : user does not confirm action
  }
```
