FROM golang:1.19.2 AS builder

WORKDIR /app
COPY . .

ENV GO111MODULE=on
ARG GOARCH=amd64
RUN GOOS=linux go build -v -o main .

FROM alpine as production

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /app/main /
RUN apk add --no-cache gcompat

CMD ["/main"]
