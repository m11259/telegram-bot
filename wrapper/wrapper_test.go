package wrapper

import (
	"testing"
)

func TestWrap(t *testing.T) {
	cases := []struct {
		format string
		args   map[string]interface{}
		result string
	}{
		{
			format: "{key1}",
			args: map[string]interface{}{
				"key1": 5,
			},
			result: "5",
		},
		{
			format: "And some text {key1} by sides",
			args: map[string]interface{}{
				"key1": 5,
			},
			result: "And some text 5 by sides",
		},
		{
			format: "And with {key1} several {key2} keys",
			args: map[string]interface{}{
				"key1": 5,
				"key2": 10,
			},
			result: "And with 5 several 10 keys",
		},
		{
			format: "And with {key1} several {key2} keys and at the end {12}",
			args: map[string]interface{}{
				"key1": 5,
				"key2": 10,
				"12":   12222,
			},
			result: "And with 5 several 10 keys and at the end 12222",
		},
		{
			format: "{key1} and start and end {key2}",
			args: map[string]interface{}{
				"key1": 5,
				"key2": 10,
			},
			result: "5 and start and end 10",
		},
		{
			format: "No such key {nosuchkey}",
			args:   map[string]interface{}{},
			result: "No such key <nil>",
		},
		{
			format: "No such key {nosuchkey} here",
			args:   map[string]interface{}{},
			result: "No such key <nil> here",
		},
		{
			format: "{nosuchkey} No such key {nosuchkey}",
			args:   map[string]interface{}{},
			result: "<nil> No such key <nil>",
		},
		{
			format: "Unfinished template {unfinished",
			args: map[string]interface{}{
				"unfinished": "finished",
			},
			result: "Unfinished template {unfinished",
		},
	}
	for _, c := range cases {
		result := Wrap(c.format, c.args)
		t.Logf("wrap result: %s\n", result)
		if result != c.result {
			t.Errorf("resulted string do not match with desired. %s != %s\n", result, c.result)
		}
	}
}
