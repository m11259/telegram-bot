package wrapper

import (
	"errors"
	"fmt"
)

type WrapReader struct {
	payload []byte
	answer  []byte
	size    int
	idx     struct {
		current int
		start   int
		end     int
		copied  int
	}
}

func NewWrapReader(payload string) *WrapReader {
	return &WrapReader{
		payload: []byte(payload),
		size:    len(payload),
		answer:  make([]byte, 0),
	}
}

func (w *WrapReader) readTillOpen() error {
	if len(w.payload) == 0 {
		return errors.New("wrapper: reached end")
	}
	var openByte byte = '{'
	for w.payload[w.idx.current] != openByte {
		if w.idx.current+1 < w.size {
			w.idx.current++
		} else {
			return errors.New("wrapper: reached end")
		}
	}
	w.idx.start = w.idx.current
	return nil
}

func (w *WrapReader) readTillClose() error {
	var closeByte byte = '}'
	for w.payload[w.idx.current] != closeByte {
		if w.idx.current+1 < w.size {
			w.idx.current++
		} else {
			return errors.New("wrapper: reached end")
		}
	}
	w.idx.end = w.idx.current
	return nil
}

func (w *WrapReader) NextKey() (string, error) {
	if err := w.readTillOpen(); err != nil {
		return "", err
	}
	if err := w.readTillClose(); err != nil {
		return "", err
	}
	return string(w.payload[w.idx.start+1 : w.idx.end]), nil
}

func (w *WrapReader) Set(value []byte) {
	w.answer = append(w.answer, w.payload[w.idx.copied:w.idx.start]...)
	w.answer = append(w.answer, value...)
	w.idx.copied = w.idx.end + 1
}

func (w *WrapReader) Result() []byte {
	w.answer = append(w.answer, w.payload[w.idx.copied:]...)
	return w.answer
}

func Wrap(format string, args map[string]interface{}) string {
	wreader := NewWrapReader(format)
	var key, value string
	var err error
	for {
		key, err = wreader.NextKey()
		if err != nil {
			break
		}
		value = fmt.Sprint(args[key])
		wreader.Set([]byte(value))
	}
	return string(wreader.Result())
}
