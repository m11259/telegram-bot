package cache

import (
	"gitlab.com/m11259/telegram-bot/logger"
	"gitlab.com/m11259/telegram-bot/repository"

	omsv1 "go.buf.build/grpc/go/aipyth/ongate-oms/oms/v1"
)

type Cache struct {
	db     *repository.Services
	values map[int64]string
}

func NewCache(db *repository.Services) *Cache {
	values := make(map[int64]string, 0)
	return &Cache{
		db,
		values,
	}
}

func (c *Cache) GetLang(id int64, defaultLanguage ...string) (string, error) {
	if lang, ok := c.values[id]; ok {
		return lang, nil
	} else {
		res, err := c.db.User.GetUser(repository.Context(), &omsv1.GetUserRequest{
			Id: &id,
		})
		logger.Debugf("[Cache] getting user lang %d: %v | %v \n", res.GetId(), res.GetLang(), err)
		if err != nil {
      // if len(defaultLanguage) != 0 {
      //   c.values[id] = defaultLanguage[0]
      // }
			return c.values[id], err
		}
		c.values[id] = res.GetLang()
		return c.values[id], nil
	}
}

func (c *Cache) SetLang(id int64, lang string) {
	_, err := c.db.User.UpdateUser(repository.Context(), &omsv1.UpdateUserRequest{
		Id:   id,
		Lang: &lang,
	})
	if err != nil {
		logger.Error(err)
	}
	c.values[id] = lang
}
