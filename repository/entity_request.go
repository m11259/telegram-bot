package repository

import omsv1 "go.buf.build/grpc/go/aipyth/ongate-oms/oms/v1"

type Request struct {
	*omsv1.Request
}

func (r Request) GetUserId() int64 {
	if r.User != nil {
		return r.User.GetId()
	}
  return 0
}
