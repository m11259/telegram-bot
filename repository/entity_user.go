package repository

import omsv1 "go.buf.build/grpc/go/aipyth/ongate-oms/oms/v1"

type User struct {
	*omsv1.User
}

func (u User) GetUserId() int64 {
	return u.GetId()
}
