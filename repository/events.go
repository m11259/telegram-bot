package repository

import (
	"encoding/json"
	"errors"

	omsv1 "go.buf.build/grpc/go/aipyth/ongate-oms/oms/v1"
	"google.golang.org/grpc"
)

type Event string

const (
	UserCreated    Event = "user/create"
	UserApproved   Event = "user/identify"
	UserDenied     Event = "user/deny"
	UserActivated  Event = "user/activate"
	UserDectivated Event = "user/deactivate"

	RequestAccepted   Event = "request/accept"
	RequestUpdated    Event = "request/update"
	RequestDenied     Event = "request/deny"
	RequestUnaccepted Event = "request/unaccept"
	RequestUndenied   Event = "request/undeny"
	RequestCreated    Event = "request/create"
	RequestCancel     Event = "request/cancel"
)

var topics = []string{
	string(UserApproved),
	string(UserDenied),
	string(UserActivated),
	string(UserDectivated),
	string(RequestAccepted),
	string(RequestDenied),
	string(RequestUpdated),
	string(RequestUnaccepted),
	string(RequestUndenied),
}

type EventService struct {
	omsv1.EventServiceClient
}

func NewEventService(conn grpc.ClientConnInterface) EventService {
	return EventService{omsv1.NewEventServiceClient(conn)}
}

func (e *EventService) Emit(topic Event, data any) error {
	serialized, err := json.Marshal(data)
	if err != nil {
		return err
	}
	_, err = e.Push(Context(), &omsv1.PushRequest{
		Topic: string(topic),
		Data:  string(serialized),
	})
	return err
}

type EventData interface {
	GetUserId() int64
}

type Entity interface {
	User | Request
}

type EventUpdate struct {
	Event string
	Id    int64
	Data  EventData
}

func parseEventUpdate[Ent Entity](msg *omsv1.SubscribeResponse) (*EventUpdate, error) {
	var parsed Ent
	// PERF: use a faster parsing algorithm github.com/json-iterator/go
	err := json.Unmarshal([]byte(msg.GetData()), &parsed)
	if err != nil {
		return nil, err
	}
	eventData, ok := any(parsed).(EventData)
	if !ok {
		return nil, errors.New("unparsable type")
	}
	return &EventUpdate{
		Event: msg.GetTopic(),
		Id:    eventData.GetUserId(),
		Data:  eventData,
	}, nil
}
