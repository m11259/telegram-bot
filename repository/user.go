package repository

import (
	omsv1 "go.buf.build/grpc/go/aipyth/ongate-oms/oms/v1"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GetUserOpts struct {
	Id       int64
	Username string
}

func (r *Services) GetUser(opts GetUserOpts) (*omsv1.User, codes.Code, error) {
	req := omsv1.GetUserRequest{}
	if opts.Id != 0 {
		req.Id = &opts.Id
	} else {
		req.Username = &opts.Username
	}
	user, err := r.User.GetUser(Context(), &req)
	st, ok := status.FromError(err)
	if !ok {
		return nil, codes.Internal, err
	}
	return user, st.Code(), nil
}
