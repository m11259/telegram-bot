package repository

import (
	"context"
	"io"
	"strings"

	"gitlab.com/m11259/telegram-bot/logger"
	omsv1 "go.buf.build/grpc/go/aipyth/ongate-oms/oms/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/metadata"
)

const clientName = "Telegram-Bot"

type Services struct {
	Request     omsv1.RequestServiceClient
	User        omsv1.UserServiceClient
	Address     omsv1.AddressServiceClient
	TextStorage omsv1.TelegramTextStorageServiceClient
	Event       EventService
}

func NewServices(url string) (*Services, error) {
	logger.Infof("Connecting to OMS host %s\n", url)
	conn, err := grpc.Dial(url, grpc.WithTransportCredentials(insecure.NewCredentials()))
	return &Services{
		Request:     omsv1.NewRequestServiceClient(conn),
		User:        omsv1.NewUserServiceClient(conn),
		Address:     omsv1.NewAddressServiceClient(conn),
		TextStorage: omsv1.NewTelegramTextStorageServiceClient(conn),
		Event:       NewEventService(conn),
	}, err
}

func Context() context.Context {
	return metadata.AppendToOutgoingContext(context.Background(), "grpc-caller-service", clientName)
}

func (s *Services) ConnectEvents() (chan EventUpdate, error) {
	stream, err := s.Event.Subscribe(context.Background())
	if err != nil {
		return nil, err
	}
	channel := make(chan EventUpdate)
	stream.Send(&omsv1.SubscribeRequest{
		ClientName: clientName,
		Topic:      topics,
	})
	go s.processEventsIncomingUpdates(channel, stream)
	return channel, nil
}

func (s *Services) processEventsIncomingUpdates(channel chan EventUpdate, stream omsv1.EventService_SubscribeClient) {
	for {
		msg, err := stream.Recv()
		if err == io.EOF {
			logger.Errorf("EventStream exhaused with error %v\n", err)
			// TODO: safe CloseSend with mutex for rw as there is SendMsg method in this stream
			// stream.CloseSend
			logger.Fatal(err)
			// TODO: safely reconnect with transient channel change
			// logger.Infof("Reconnecting to EventService\n")
		}
		if err != nil {
			logger.Fatal(err)
			// TODO: safely reconnect with transient channel change
			// logger.Infof("Reconnecting to EventService\n")
		}

		processEvent(msg, channel)
	}
}

func processEvent(msg *omsv1.SubscribeResponse, channel chan EventUpdate) {
	topicPath := strings.Split(msg.GetTopic(), "/")
	switch topicPath[0] {
	case "user":
		update, err := parseEventUpdate[User](msg)
		if err != nil {
			logger.Errorf("%v\n", err)
			return
		}
		channel <- *update
	case "request":
		update, err := parseEventUpdate[Request](msg)
		if err != nil {
			logger.Errorf("%v\n", err)
			return
		}
		channel <- *update
	}
}
