package repository

import (
	omsv1 "go.buf.build/grpc/go/aipyth/ongate-oms/oms/v1"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UserRequestData struct {
	Body      string
	Address   string
	UserId    int64
	CarNumber *string
}

func (s *Services) SendRequest(request UserRequestData) (*omsv1.Request, codes.Code, error) {
	reqOms := &omsv1.CreateRequestRequest{
		UserId:  &request.UserId,
		Body:    request.Body,
		Address: request.Address,
	}
  if request.CarNumber != nil {
    reqOms.CarNumber = request.CarNumber
  }

	req, err := s.Request.CreateRequest(Context(), reqOms)
	st, ok := status.FromError(err)
	if !ok {
		return nil, codes.Internal, err
	}

	s.Event.Emit(RequestCreated, req)

	return req, st.Code(), nil
}
