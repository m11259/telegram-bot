package logger

import (
	"fmt"
	"os"
	"time"
)

var SupressDebug bool = false

func formatNow() string {
	return time.Now().Format(" - Jan 2 15:04:05 MST - ")
}

func Println(v ...interface{}) {
	fmt.Print("  API  ", formatNow())
	fmt.Println(v...)
}

func Printf(format string, v ...interface{}) {
	fmt.Print("  API  ", formatNow())
	fmt.Printf(format, v...)
	// fmt.Print("\n")
}

func Info(v ...interface{}) {
	fmt.Print(" INFO  ", formatNow())
	fmt.Println(v...)
}

func Debug(v ...interface{}) {
	if SupressDebug {
		return
	}
	fmt.Print(" DEBUG ", formatNow())
	fmt.Println(v...)
}

func Error(v ...interface{}) {
	fmt.Print(" ERROR ", formatNow())
	fmt.Println(v...)
}

func Infof(format string, v ...interface{}) {
	fmt.Print(" INFO  ", formatNow())
	fmt.Printf(format, v...)
}

func Debugf(format string, v ...interface{}) {
	if SupressDebug {
		return
	}
	fmt.Print(" DEBUG ", formatNow())
	fmt.Printf(format, v...)
}

func Errorf(format string, v ...interface{}) {
	fmt.Print(" ERROR ", formatNow())
	fmt.Printf(format, v...)
}

func Fatal(v ...interface{}) {
	fmt.Print(" FATAL ", formatNow())
	fmt.Println(v...)
	os.Exit(1)
}

func Warning(v ...interface{}) {
	fmt.Print(" WARN  ", formatNow())
	fmt.Println(v...)
}
