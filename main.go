package main

import (
	"os"

	"log"

	"gitlab.com/m11259/telegram-bot/cache"
	"gitlab.com/m11259/telegram-bot/config"
	"gitlab.com/m11259/telegram-bot/domain"
	"gitlab.com/m11259/telegram-bot/handler"
	"gitlab.com/m11259/telegram-bot/logger"
	"gitlab.com/m11259/telegram-bot/repository"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

var botToken = os.Getenv("BOT_TOKEN")

func main() {
	bot, err := tgbotapi.NewBotAPI(botToken)
	if err != nil {
		logger.Fatal("creating bot:", err)
	}

	s, err := repository.NewServices(os.Getenv("GRPC_URL"))
	if err != nil {
		logger.Fatal("Error connecting to rpc:", err)
	}
	cfg, err := config.GetConfig()
	if err != nil {
		panic(err)
	}

	bot.Debug = cfg.TGBotApiDebug
  log.Printf("using debug %v\n", cfg.Debug)
	logger.SupressDebug = !cfg.Debug

	logger.Infof("Authorized on account %s\n", bot.Self.UserName)

	sigmaConfig, err := config.GetSigmaConfig()
	if err != nil {
		panic(err)
	}

	sgm := config.NewSigma(sigmaConfig, s.TextStorage)
	if cfg.Sigma.UseRedis && (!sgm.IsPopulated() || cfg.Sigma.PopulateOnStart) {
		logger.Info("Populating Redis with Sigma Config")
		sgm.Populate()
	}

	hdl := handler.New()
	csh := cache.NewCache(s)

	dom := domain.New(domain.DomainOps{
		Handler:    hdl,
		Repository: s,
		Config:     cfg,
		Sigma:      sgm,
		Cache:      csh,
	})
	dom.Configure(cfg)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	tgUpdates, err := bot.GetUpdatesChan(u)
	eventUpdates, err := s.ConnectEvents()
	if err != nil {
		log.Fatal(err)
	}
	updates := make(chan handler.Update)
	go func() {
		for {
			select {
			case update := <-eventUpdates:
				log.Printf("GOT OMS NOTE: %+v", update)
				updates <- handler.Update{EventUpdate: &update}

			case update := <-tgUpdates:
				updates <- handler.Update{Update: &update}
			}
		}
	}()

	for update := range updates {
		ctx := handler.NewCtx(bot, update, csh, sgm)
		hdl.ProcessUpdate(bot, update, ctx)
	}
}
