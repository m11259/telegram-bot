module gitlab.com/m11259/telegram-bot

go 1.19

require (
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	go.buf.build/grpc/go/aipyth/ongate-oms v1.4.16
	google.golang.org/grpc v1.50.1
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	golang.org/x/net v0.0.0-20221017152216-f25eb7ecb193 // indirect
	golang.org/x/sys v0.1.0 // indirect
	golang.org/x/text v0.4.0 // indirect
	google.golang.org/genproto v0.0.0-20221014213838-99cd37c6964a // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
